const { factorial, div_check } = require('../src/util.js');

const { expect, assert } = require('chai');
//gets the expect and assert functions from chai to be used

//accepts two parameters - a string explaining what the test should do, and a callback function which contains the 'actual test'

//ASSERT: Fails fast, aborting the current function. EXPECT: Continues after the failure.

//a test case or a unit test is a single description about the desired behavior of a code that either passes or fails

//test suites are made up of collection of test cases that should be executed together. the "describe" keyword is used to group tests together

//indicator yung first

describe('test_fun_factorials', ()=>{
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0)
		assert.equal(product, 1)
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4)
		expect(product).to.equal(24)
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10)
		expect(product).to.equal(3628800)
	})
	
})

describe('test_divisibilty_by_5_or_7', ()=>{

	it('test_100_is_divisible_by_5', () => {
		const number = div_check(100);
		expect(number).to.equal(true);
	})

	it('test_49_is_divisible_by_7', () => {
		const number = div_check(49);
		expect(number).to.equal(true);
	})	


	it('test_30_is_divisible_by_5', () => {
		const number = div_check(30);
		expect(number).to.equal(true);
	})

	it('test_56_is_divisible_by_7', () => {
		const number = div_check(56);
		expect(number).to.equal(true);
	})	

})

